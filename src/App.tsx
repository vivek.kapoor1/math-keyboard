import React, { useEffect, useRef, useState } from 'react';
import KeyboardJS from 'keyboardjs';
import katex from 'katex';
import 'katex/dist/katex.min.css';
import './App.css';

interface MathKeyboardProps {
  onSymbolSelected: (symbol: string) => void;
}

const MathKeyboard: React.FC<MathKeyboardProps> = ({ onSymbolSelected }) => {
  const keyboardRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    KeyboardJS.bind('backspace', () => {
      onSymbolSelected('\\backspace');
    });

    KeyboardJS.bind('enter', () => {
      onSymbolSelected('\\enter');
    });

    // Bind other keys as needed

    return () => {
      KeyboardJS.reset();
    };
  }, [onSymbolSelected]);

  const handleSymbolClick = (symbol: string) => {
    onSymbolSelected(symbol);
  };

  return (
    <div className="keyboard" ref={keyboardRef}>
      <div className="keyboard-row">
        <button onClick={() => handleSymbolClick('\\alpha')}>α</button>
        <button onClick={() => handleSymbolClick('\\beta')}>β</button>
        {/* Add more buttons for basic symbols */}
      </div>
      <div className="keyboard-row">
        <button onClick={() => handleSymbolClick('\\int')}>∫</button>
        <button onClick={() => handleSymbolClick('\\sum')}>Σ</button>
        {/* Add more buttons for calculus symbols */}
      </div>
      <div className="keyboard-row">
        <button onClick={() => handleSymbolClick('\\angle')}>∠</button>
        <button onClick={() => handleSymbolClick('\\triangle')}>△</button>
        {/* Add more buttons for geometry symbols */}
      </div>
    </div>
  );
};

interface AppProps {}

const App: React.FC<AppProps> = () => {
  const [editorContent, setEditorContent] = useState('');

  const handleSymbolSelected = (symbol: string) => {
    let updatedContent = editorContent;

    if (symbol === '\\backspace') {
      updatedContent = updatedContent.slice(0, -1);
    } else if (symbol === '\\enter') {
      updatedContent += '\n';
    } else {
      updatedContent += symbol;
    }

    setEditorContent(updatedContent);
  };

  return (
    <div className="App">
      <h1>Math Keyboard</h1>
      <div className="editor-container">
        <div className="editor">
          <textarea
            value={editorContent}
            onChange={(e) => setEditorContent(e.target.value)}
            placeholder="Type your math equations here..."
          />
          <div className="preview">
            {katex.renderToString(editorContent, { throwOnError: false })}
          </div>
        </div>
        <MathKeyboard onSymbolSelected={handleSymbolSelected} />
      </div>
    </div>
  );
};

export default App;
